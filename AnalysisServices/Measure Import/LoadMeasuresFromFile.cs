// Script: Generate Base Measures.cs

    bool enableDaxFormatter = true;

    var rootFolder = "C:\\Users\\wjame\\OneDrive\\Documents\\BitBucket\\GenerateCapital\\operationsdatamodel\\AnalysisServices\\";

    var metaDataPatternFile = rootFolder + "PowerBI_Measures_ForImport.txt";
    var metaDataPattern = ReadFile(metaDataPatternFile);



//Delete all AUTO-GEN measures
foreach(var t in Model.Tables.ToList())
{
    foreach(var m in t.Measures.Where(m => m.GetAnnotation("AUTO-GEN-BASE") == "1").ToList())
    {
        m.Delete();

    }
}


//MetaData File Prep
var patternRows = metaDataPattern.Split(new [] { '\r'}, StringSplitOptions.RemoveEmptyEntries);


// -- Begin: BasePattern -----------------------------------------------------------------------------------------

    //Loop: Pattern
    foreach(var patternRow in patternRows.Skip(1))
    {


        var patternColumns = patternRow.Split('\t');
        if (patternColumns.Count() == 5) {
            var table = patternColumns[0].Trim();
            var displayFolder = patternColumns[1].Trim();
            var measure = patternColumns[2].Trim();
            var expression = patternColumns[3].Trim();
            var format_string = patternColumns[4].Trim();


            //Remove Repeated Double-Quotes (usually around values)
            expression = expression.Replace("\u0022\u0022", "\u0022");


            // Remove Leading/Trailing Quotes
            if (expression[0] == (char)34 && expression[expression.Length - 1] == (char)34)
            {
                expression = expression.Substring(1, expression.Length - 2);
            }
            
            if (format_string.Length > 2)
            {
                if (format_string[0] == (char)34 && format_string[format_string.Length - 1] == (char)34)
                {
                    format_string = format_string.Substring(1, format_string.Length - 2);
                }
            }
            

            if (enableDaxFormatter == true) {
                expression = FormatDax(expression);
            }

            // Check if Table Exists and Create if it Does Not
            if (!Model.Tables.Contains(table)) {
                var objNewTable = Model.AddTable(table);
                objNewTable.Partitions[0].Query = "SELECT 1 as PlaceHolder";
                objNewTable.AddDataColumn("PlaceHolder");
            }

            // Manipulate Model (Add Measure)
            var objTable = Model.Tables[table];
            var objMeasure = objTable.AddMeasure(measure);

            objMeasure.DisplayFolder = displayFolder;
            objMeasure.Expression = expression;
            objMeasure.SetAnnotation("AUTO-GEN-BASE", "1");
            objMeasure.FormatString = format_string;
            //objMeasure.IsHidden = (visible == "FALSE");
        }

}

// -- End: BasePattern --------------------------------------------------------------------------------------------
