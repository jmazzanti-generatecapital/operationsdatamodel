CREATE OR REPLACE VIEW DWH.PUBLIC.VWSOURCE_STAGEOPERATIONS_SOLAR_FORECAST_BASELINE_ORIGINAL

AS

-- Forecast Solar Measures (VIEW_FORECAST_BASELINE) -- 

-- SOLAR_FC_POABASE
SELECT 
    'SOLAR' 		                    AS ASSETTYPEID,
     date_trunc('DAY',F.LOCAL_MONTH)    AS DATE,
    CONCAT(D.VENDORID,'_SOLAR_SITE')          AS DEVICEID,
    'SOLAR_SITE'            AS DEVICETYPEID,
    D.LEGALENTITYID         AS LEGALENTITYID,
    'SOLAR_FC_POABASE' 	    AS METRICID,
    null		    AS OPPORTUNITYID,
    TO_VARCHAR(F.PROJECT_CODE) 	    AS PROJECTID,
    'FORECAST' 	    	            AS SCENARIOID,
    D.SITEID     	                AS SITEID,
    to_time('23:59:59')             AS TIMEID,
    D.VENDORID 	                    AS VENDORID,
    POA  	                        AS NUMERIC_VALUE,
    null 		                    AS TEXT_VALUE,
    CURRENT_TIMESTAMP()             AS GC_LOAD_TIMESTAMP
FROM 
    DATAWAREHOUSE.PUBLIC.VIEW_FORECAST_BASELINE  F
    -- Dimensions (LegalEntity, Site)
    INNER JOIN
    (
     SELECT 
       D.PROJECTID              AS PROJECTID,
       D.VENDORID	            AS VENDORID,
       MAX(D.LEGALENTITYID)     AS LEGALENTITYID,
       MAX(D.SITEID)            AS SITEID
    FROM
        DWH.PUBLIC.VWDIMENSIONS D
        INNER JOIN 
        DWH.PUBLIC.VWSOURCE_DIMVENDOR V
        ON 
        D.VENDORID = V.VENDORID
     GROUP BY 
       D.VENDORID,
       D.PROJECTID
    )
    D
    ON
    D.PROJECTID = F.PROJECT_CODE

UNION ALL 

-- SOLAR_FC_GENBASE
SELECT 
    'SOLAR' 		    AS ASSETTYPEID,
     date_trunc('DAY',F.LOCAL_MONTH)    AS DATE,
    CONCAT(D.VENDORID,'_SOLAR_SITE')          AS DEVICEID,
    'SOLAR_SITE'        AS DEVICETYPEID,
    D.LEGALENTITYID     AS LEGALENTITYID,
    'SOLAR_FC_GENBASE'	AS METRICID,
    null		        AS OPPORTUNITYID,
    TO_VARCHAR(F.PROJECT_CODE) 	AS PROJECTID,
    'FORECAST' 	    	AS SCENARIOID,
    D.SITEID    	    AS SITEID,
    to_time('23:59:59') AS TIME,
    D.VENDORID 	        AS VENDORID,
    ENERGY_GRID_KWH     AS NUMERIC_VALUE,
    null 		        AS TEXT_VALUE,
    CURRENT_TIMESTAMP() AS GC_LOAD_TIMESTAMP
FROM 
    DATAWAREHOUSE.PUBLIC.VIEW_FORECAST_BASELINE  F
    -- Dimensions (LegalEntity, Site)
    INNER JOIN
    (
     SELECT 
       D.PROJECTID           AS PROJECTID,
       D.VENDORID	         AS VENDORID,
       MAX(D.LEGALENTITYID)  AS LEGALENTITYID,
       MAX(D.SITEID)         AS SITEID
    FROM
        DWH.PUBLIC.VWDIMENSIONS D
     GROUP BY 
       D.VENDORID,
       D.PROJECTID
    )
    D
    ON
    D.PROJECTID = F.PROJECT_CODE

