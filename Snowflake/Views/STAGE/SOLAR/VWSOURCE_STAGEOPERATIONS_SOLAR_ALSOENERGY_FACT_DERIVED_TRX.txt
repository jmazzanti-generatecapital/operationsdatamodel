CREATE OR REPLACE VIEW DWH.PUBLIC.VWSOURCE_STAGEOPERATIONS_SOLAR_ALSOENERGY_FACT_DERIVED_TRX

AS

/*
DERIVES PERIOD VALUES USING FIRST_VALUE AND LAST_VALUE CUMULATIVE VALUES FROM FACT TABLE
*/


SELECT DISTINCT
    'DERIVED' AS TYPE,
    'SOLAR' AS ASSETTYPEID,
    DATE_LOCAL AS DATE,
    '00:00:00' AS TIME,
   'ALSOENERGY' AS VENDORID,
    SITEID,
    DEVICEID,
    ROUND(LAST_VALUE,4) - ROUND(FIRST_VALUE,4) AS NUMERIC_VALUE,
    DATA_NAME
FROM
(
SELECT
        DATE_TRUNC('DAY',ITEMS_TIMESTAMP)         AS DATE,
        T.TIMEID               		          AS TIME,

         DATE_TRUNC('DAY',CONVERT_TIMEZONE('GMT',tz.TZ_DATABASE_NAME
                               , to_timestamp(to_char(F.ITEMS_TIMESTAMP,'yyyy-mm-dd HH:mi:SS'))  
                              ))    DATE_LOCAL,

  
        to_time(date_trunc('SECOND', CONVERT_TIMEZONE('GMT',tz.TZ_DATABASE_NAME
                               , to_timestamp(to_char(F.ITEMS_TIMESTAMP,'yyyy-mm-dd HH:mi:SS'))  
                              )))    TIME_LOCAL,

        TO_VARCHAR(F.SITE_ID)                 AS SITEID, 
        TO_VARCHAR(F.HARDWARE_ID)             AS DEVICEID,
        DATA_VALUE                            AS DATA_VALUE,
        FIRST_VALUE(DATA_VALUE)   OVER (PARTITION BY DATA_NAME, SITEID, DEVICEID, DATE_LOCAL  ORDER BY  DATA_NAME,SITEID,DEVICEID,DATE_LOCAL,TIME_LOCAL) AS FIRST_VALUE,
        LAST_VALUE(DATA_VALUE)    OVER (PARTITION BY DATA_NAME, SITEID, DEVICEID, DATE_LOCAL  ORDER BY  DATA_NAME,SITEID,DEVICEID,DATE_LOCAL,TIME_LOCAL) AS LAST_VALUE,
        DATA_NAME, 
        D.HARDWARE_FUNCTIONCODE
     FROM 
      DATAWAREHOUSE.PUBLIC.RAW_ALSOENERGY_METRICS2 F
	  LEFT JOIN 
      DATAWAREHOUSE.PUBLIC.LU_RAW_ALSO_ENERGY_HARDWARE D
      ON
      D.SITE_ID::string = F.SITE_ID::string
      AND
      D.HARDWARE_ID::string = F.HARDWARE_ID::string

      LEFT JOIN 
      datawarehouse.public.lu_raw_also_energy_site_details  lusite
      ON
      F.SITE_ID = lusite.SITE_ID
      LEFT JOIN
      datawarehouse.public.lu_static_raw_timezone  tz
      on 
      lusite.TIMEZONE_DISPLAYNAME = tz.ALSOENERGY_VALUES

      LEFT JOIN 
      DWH.PUBLIC.VWSOURCE_DIMTIME T
      ON 
      to_time(date_trunc('SECOND', ITEMS_TIMESTAMP)) = T.TIMEID
  WHERE
    F.DATA_NAME IN 
    (
    'KWHnet',
    'KWHdel',
    'KWHrec'
    )
    AND
    IFNULL(F.DATA_VALUE,0) <> 0      
)
ORDER BY
    DATA_NAME,
    DATE,
    SITEID,
    DEVICEID,
    TIME
