CREATE OR REPLACE FUNCTION FNGENERATEDATERANGE(startDate DATE, yearsForwardFromToday INT)

returns table(CALENDARDATE date)

as

$$

  SELECT startDate AS CalendarDate
  
  UNION ALL 
   
  SELECT 
      DATEADD(day,x.rn,startDate) AS CalendarDate
  FROM       
    (SELECT ROW_NUMBER() OVER (ORDER BY 1) AS rn FROM table(generator(rowcount => (SELECT DATEDIFF(day,startDate,(SELECT TO_DATE(CONCAT('12/31/',TRIM(TO_CHAR(YEAR(DATEADD(year,yearsForwardFromToday,CURRENT_DATE())),'0000'))))))))) ) AS x
 
        
$$

